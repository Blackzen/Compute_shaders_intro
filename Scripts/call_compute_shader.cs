﻿using UnityEngine;
//in order to calculte data size we will use Marshal
using System.Runtime.InteropServices;
//creating structure to handle info that want to be sent to the GPU (in this case only a position)
struct Point
{
    public Vector3 position;
}

public class call_compute_shader : MonoBehaviour {
    [Header("Compute Shader Example")]

    [Tooltip("compute shader to be used in calculations")]
    public ComputeShader shader;

    [Tooltip("Object to be instanciated")]
    public GameObject prefab;

    [Tooltip("Object count")]
    public int count = 10;

    [Header("Data Buffers to read and write data")]
    private ComputeBuffer _object_data_buffer;

    //objects and structs to store object info
    private GameObject[] _Objects;
    private Point[] _pointStructs;

    // Use this for initialization
    void Start () {
        //defining array size to be filled
        _Objects = new GameObject[count];
        _pointStructs = new Point[count];

        //generating the number of objects you defined in count
        for (int i = 0; i < count; i++)
        {
            //generating random position inide a sphere using GetPoint()
            _pointStructs[i] = GetPoint();
            //instanciating and changing property then store it for later use example (update position)
            _Objects[i] = GetObject(_pointStructs[i]);
        }

        //calculating structure size in bytes using Marshal from System.Runtime.InteropServices
        int size = Marshal.SizeOf(new Point());
        //creating buffer for all the object using the count and the amount that each will take in bytes
        _object_data_buffer = new ComputeBuffer(count,size);
        //data will be put in to "computers" buffer try not to miss this step
        _object_data_buffer.SetData(_pointStructs);
	}
	
	// Update is called once per frame
	void Update () {
        //finding kernel inside the shader by name, in this case called CSMain
        int kernelHandle = shader.FindKernel("CSMain");
        //this step will put your data in to the "GPU" buffer using the name given there to the RWStructuredBuffer<T> 
        shader.SetBuffer(kernelHandle,"_object_data_buffer",_object_data_buffer);
        //this will run your kernel and perform the written operations
        shader.Dispatch(kernelHandle,count,1,1);
        //reading data from the buffer after running the kernel (HERE IS OUR UPDATED DATA) =)  
        _object_data_buffer.GetData(_pointStructs);
        //updating objects position (there is ways to achieve this faster)
        for (int i = 0; i < _pointStructs.Length; i++)
        {
            _Objects[i].transform.position = _pointStructs[i].position;
        }
    }

    //making sure to free memory from 1 of the buffers (i think there are more problems here)
    private void OnDestroy()
    {
        if (_object_data_buffer != null) _object_data_buffer.Release();
    }

    //instanciates an object and changes its trailcolor only for fun (returns an object in a given position).
    private GameObject GetObject(Point point) {
        TrailRenderer tr = prefab.GetComponent<TrailRenderer>();
        Color  col = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        tr.startColor = col;
        tr.endColor = col;
        GameObject go = Instantiate(prefab, point.position, Quaternion.identity);
        return go;
    }
    //creates a random point inside a sphere and returns its position.
    private Point GetPoint() {
        Point point = new Point();
        point.position = Random.insideUnitSphere*1;
        return point; 
    }

}
